var x = 25;
var y = 50;
var direction = 5;

function setup() {
    createCanvas(640, 480);
}

function draw() {
    background(220);
    fill(255,0,0)
    rect(x, y, 50, 50);
    x=x+direction;
    y=y+direction;
    
    if (y > 430) {
        direction = -5;
    } else if (x < 0) {
        direction = 5;
    }
}