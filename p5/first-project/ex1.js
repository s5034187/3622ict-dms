function setup() {
    createCanvas(640, 480);
}

function draw() {
    fill(255, 105, 180);
    ellipse(50, 50, 100, 100);
    fill(0, 0, 255);
    rect(100, 100, 60, 30);
    line(200,5,300,5);
    triangle(130, 175, 280, 150, 250, 125);
}