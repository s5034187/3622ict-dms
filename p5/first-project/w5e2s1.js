// Author: Jed Burr
// Date: 1/08/17
// Description: Week 5 Exercise 2 Step 1
var images 			= new Array(4);
var imagesX 		= new Array(4);
var imagesY 		= new Array(4);
var imagesS 		= new Array(4);
var imageSize 		= 64;

function preload() {
    images[0] 		= loadImage('images/icon1.png');
	images[1] 		= loadImage('images/icon2.png');
	images[2] 		= loadImage('images/icon3.png');
	images[3] 		= loadImage('images/icon4.png');
}

function setup() {
	createCanvas(640, 480);
	for (var i = 0; i < imagesX.length; i++) {
    	imagesX[i] 	= random(width);
    	imagesY[i] 	= random(height);
    	imagesS[i] 	= random(-2, 2);
  	}
}

function draw() {
	background(220);
	for (var i = 0; i < imagesX.length; i++) {
		image(images[i], imagesX[i] += imagesS[i], imagesY[i]);
    
	    if (imagesX[i] > width - imageSize) {
	        imagesS[i] = imagesS[i] * -1;
	    } else if (imagesX[i] < 0) {
	        imagesS[i] = imagesS[i] * -1;
	    }

	    if((mouseIsPressed) && (mouseX >= imagesX[i] && mouseY >= imagesY[i]) && (mouseX < (imagesX + 64)  && mouseY < (imagesY[i] + 64))) {
	    	images[i] = undefined;
	    	console.log(images[i]);
	    }

	}
}