// Author: Jed Burr
// Date: 25/07/17
// Description: Week 4 Exercise 1 Step 1
var img;

function preload() {
    img = loadImage('images/sample.png');
}

function setup() {
    createCanvas(640, 480);
}

function draw() {
    image(img, 0, 0)
    background(black)
}