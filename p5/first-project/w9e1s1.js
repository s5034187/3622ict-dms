/**     Problem:
        Implement a P5 program to retrieve 20 “interesting” photos for the day from Flickr, by:
        Call flickr.interestingness.getList to obtain the photo IDs.
        For each photo ID obtained, call flickr.photos.getSizes to obtain URL to those photos.
        Once all 20 getSizes calls have returned, print out the URLs. 
**/

// Author: Jed Burr
// Date: 29/08/17
// Description: Week 9 Exercise 1 Step 1


function setup() {
    createCanvas(640, 480);
}

function draw() {

}