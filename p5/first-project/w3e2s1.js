var x = 25;
var y = 50;
var direction = 1;
var r = 0;

function setup() {
    createCanvas(640, 480);
}

function draw() {
    background(220);
    fill(r,100,100)
    rect(x, y, 50, 50);
    y=y+direction;
    
    if (y > 430) {
        direction = -1;
    } else if (y < 0) {
        direction = 1;
    }
    
    if (mouseIsPressed == false) {
        r = r+direction;
    }
    if (r >= 255) {
        direction=-1;
    }
    if (r < 0) {
        direction=1;
    }
}