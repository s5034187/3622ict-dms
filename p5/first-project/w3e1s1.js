// Author: Jed Burr
// Date: 18/07/17
// Description: Week 3 Exercise 1 Step 1
var x = 50;
var y = 50;

function setup() {
    createCanvas(640, 480);
}

function draw() {
    background(220);
    ellipse(x, y, 50, 50);
    x++;
    y++;
}