// Author: Jed Burr
// Date: 25/07/17
// Description: Week 4 Exercise 2 Step 1

function setup() {
    createCanvas(640, 480);
    background("black");
}

function draw() {
    textStyle(BOLD);
    textAlign(CENTER, BOTTOM);
    fill(0, 255, 0);
    text("Hello World", width / 2, height);
}