function setup() {
    createCanvas(640, 480);
}

function draw() {
    for (var i = 0; i<5; i++) {
        line(50, i*5+50, 100, i*5+100);
    }
}